<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Laravel</title>

  <script src="https://js.stripe.com/v3/"></script>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  <style>
  .spacer {
    margin-bottom: 24px;
  }


  .StripeElement {
    background-color: white;
    padding: 10px 12px;
    border-radius: 4px;
    border: 1px solid #ccd0d2;
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: box-shadow 150ms ease;
    transition: box-shadow 150ms ease;
  }

  .StripeElement--focus {
    box-shadow: 0 1px 3px 0 #cfd7df;
  }

  .StripeElement--invalid {
    border-color: #fa755a;
  }

  .StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
  }

  #card-errors {
    color: #fa755a;
  }
  </style>

</head>
<body>
  <div class="container">
    <div class="col-md-6 col-md-offset-3">
      <h1>Formulário de pagamento</h1>
      <div class="spacer"></div>

      @if (session()->has('success_message'))
      <div class="alert alert-success">
        {{ session()->get('success_message') }}
      </div>
      @endif

      @if(count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <form action="{{ url('/checkout') }}" method="POST" id="payment-form">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="email">Endereço de e-mail</label>
          <input type="email" class="form-control" id="email">
        </div>

        <div class="form-group">
          <label for="name_on_card">Nome do titular (como está gravado no Cartão)</label>
          <input type="text" class="form-control" id="name_on_card" name="name_on_card">
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="address">Endereço</label>
              <input type="text" class="form-control" id="address" name="address">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="city">Cidade</label>
              <input type="text" class="form-control" id="city" name="city">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="province">Estado</label>
              <input type="text" class="form-control" id="province" name="province">
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="postalcode">CEP</label>
              <input type="text" class="form-control" id="postalcode" name="postalcode">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="country">País</label>
              <input type="text" class="form-control" id="country" name="country">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="phone">Telefone</label>
              <input type="text" class="form-control" id="phone" name="phone">
            </div>
          </div>

        </div>

        {{-- <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="cc_number">Número do Cartão de Crédito</label>
              <input type="text" class="form-control" id="cc_number" name="cc_number">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="expiry">Validade</label>
              <input type="text" class="form-control" id="expiry" name="expiry">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="cvc">CVC</label>
              <input type="text" class="form-control" id="cvc" name="cvc">
            </div>
          </div>

        </div> --}}

        <div class="form-group">
          <label for="card-element">Cartão de crédito</label>
          <div id="card-element">
            <!-- um Stripe Element será inserido aqui. -->
          </div>

          <!-- Usado para exibir erros de formulário -->
          <div id="card-errors" role="alert"></div>
        </div>

        <div class="spacer"></div>

        <button type="submit" class="btn btn-success">Confirmar pagamento</button>
      </form>
    </div>
  </div>

  <script>
  (function(){
    // Crie um cliente de distribuição
    var stripe = Stripe('{{ config('services.stripe.key') }}');

    // Crie uma instância de elementos
    var elements = stripe.elements();

    var style = {
      base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Raleway", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Crie uma instância do elemento de cartão
    var card = elements.create('card', {
      style: style,
      hidePostalCode: true
    });

    // Adicione uma instância do elemento card no elemento card-card.

    <div>
    card.mount('#card-element');

    // Lidar com erros de validação em tempo real do elemento do cartão.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    // Lidar com o envio do formulário
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
      event.preventDefault();

      var options = {
        name: document.getElementById('name_on_card').value,
      }

      stripe.createToken(card, options).then(function(result) {
        if (result.error) {
          // Informar o usuário se houve um erro
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          // Envie o token para o seu servidor
          stripeTokenHandler(result.token);
        }
      });
    });

    function stripeTokenHandler(token) {
      // Insira o ID do token no formulário para que ele seja enviado ao servidor
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Envie o formulário
      form.submit();
    }
  })();
  </script>
</body>
</html>
