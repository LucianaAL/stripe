<?php

use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Cartalyst\Stripe\Exception\CardErrorException;



Route::view('/', 'checkout');
Route::view('/vue', 'checkout-vue');

Route::post('/checkout', function(Request $request) {
    // dd($request->all());

    // validação

    try {
        $charge = Stripe::charges()->create([
            'amount' => 20,
            'currency' => 'CAD',
            'source' => $request->stripeToken,
            'description' => 'Description goes here',
            'receipt_email' => $request->email,
            'metadata' => [
                'data1' => 'metadata 1',
                'data2' => 'metadata 2',
                'data3' => 'metadata 3',
            ],
        ]);

        // salve esta informação no seu banco de dados

        // BEM-SUCEDIDO
        return back()->with('success_message', 'Thank you! Your payment has been accepted.');
    } catch (CardErrorException $e) {
        // salvar informações no banco de dados para falha
        return back()->withErrors('Error! ' . $e->getMessage());
    }
});
